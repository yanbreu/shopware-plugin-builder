package plugin

import (
	"regexp"

	"github.com/iancoleman/strcase"
)

// Plugin stores the Shopware Plugin data
type Plugin struct {
	Name         string
	Class        string
	Author       string
	Link         string
	MinSwVersion string
	Subscribers  []Subscriber
	initialized  bool
}

// Subscriber is a SW Plugin Subscriber
type Subscriber struct {
	Path   string
	Name   string
	Events []string
}

// NewPlugin creates a new Plugin struct
func NewPlugin() *Plugin {
	return &Plugin{initialized: false}
}

var sanitizeStringRegex = regexp.MustCompile("[^a-zA-Z0-9_ -]+")

// Initialize initializes the plugin struct and add some important missing fields.
func (p *Plugin) Initialize() {
	p.Class = sanitizeStringRegex.ReplaceAllString(p.Name, "")
	p.Class = strcase.ToCamel(p.Class)
	if len(p.MinSwVersion) == 0 {
		p.MinSwVersion = "5.2.0"
	}
	p.initialized = true
}

// IsInitialized checks if a plugin is initialized
func (p *Plugin) IsInitialized() bool {
	return p.initialized
}
