<?php

namespace {{ .Plugin.Class }}\Subscriber\{{ .Path }};

use Enlight\Event\SubscriberInterface;
use Shopware\Components\DependencyInjection\Container;

class {{ .Name }} implements SubscriberInterface
{
    /** 
     * @var Container $container 
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
{{ range $event, $func := .Events }}            "{{ $event }}" => "{{ $func }}",
{{ end }}        ];
    }
{{ range $event, $func := .Events }}
    /**
     * Event: {{ $event }}
     *
     * @param \Enlight_Event_EventArgs $args
     */
    public function {{ $func }}(\Enlight_Event_EventArgs $args)
    {

    }
    {{ end }}
}