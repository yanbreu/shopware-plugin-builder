<?xml version="1.0" ?>

<container xmlns="http://symfony.com/schema/dic/services"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://symfony.com/schema/dic/services http://symfony.com/schema/dic/services/services-1.0.xsd">

    <services>
{{ $pluginClass := .Class }}{{ range .Subscribers }}
        <service id="{{ $pluginClass | toSnake }}.subscriber.{{ .Path | toSnake }}.{{ .Name | toSnake }}"
                 class="{{ $pluginClass }}\Subscriber\{{ .Path }}\{{ .Name }}">
            <argument type="service" id="service_container"/>
            <tag name="shopware.event_subscriber"/>
        </service>
        {{ end }}
    </services>

</container>