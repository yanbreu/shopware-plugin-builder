<?xml version="1.0" encoding="utf-8"?>
<plugin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="../../../engine/Shopware/Components/Plugin/schema/plugin.xsd">

    <label>{{ .Name }}</label>

    <version>0.0.1</version>
    <link>{{ .Link }}</link>
    <author>{{ .Author }}</author>
    <compatibility minVersion="{{ .MinSwVersion }}"/>

    <changelog version="0.0.1">
        <changes>Initial Version</changes>
        <changes lang="de">Initial Version</changes>
    </changelog>

</plugin>