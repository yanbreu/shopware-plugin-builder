<?php

namespace {{ .Class }};

use Shopware\Components\Plugin;

/**
 * Shopware-Plugin {{ .Name }}.
 */
class {{ .Class }} extends Plugin
{

}