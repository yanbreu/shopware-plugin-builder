package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/yanbreu/shopware-plugin-builder/plugin"
	"gitlab.com/yanbreu/shopware-plugin-builder/pluginbuilder"
)

func main() {

	plugin := plugin.Plugin{
		Name:   "Test Plugin",
		Author: "Yannick",
		Link:   "https://gitlab.com/yanbreu",
		Subscribers: []plugin.Subscriber{
			plugin.Subscriber{Name: "Account", Path: "Frontend", Events: []string{"Shopware_Controllers_Frontend_Account::resetPasswordAction::replace", "Shopware_Abc_Def_GhIk"}},
			plugin.Subscriber{Name: "Account", Path: "Backend"},
		},
	}
	plugin.Initialize()
	builder := pluginbuilder.NewPluginBuilder(plugin)
	builder.Build()
	err := builder.Zip()
	if err != nil {
		log.Println(err)
	}
	//err = builder.CleanUp()
	//log.Println(err)

}
