package pluginbuilder

import (
	"io/ioutil"

	"os"
	"path/filepath"
	"text/template"

	log "github.com/sirupsen/logrus"

	"github.com/gobuffalo/packr"

	"github.com/iancoleman/strcase"

	"gitlab.com/yanbreu/shopware-plugin-builder/plugin"
)

// TmpDir defines the path to tmp dir
const TmpDir = "tmp"

// PluginBuilder stores data for building the plugin
type PluginBuilder struct {
	path     string
	rootPath string
	tplBox   packr.Box
	plugin   plugin.Plugin
	zipFile  string
}

// NewPluginBuilder creats a new PluginBuilder
func NewPluginBuilder(p plugin.Plugin) *PluginBuilder {
	if !p.IsInitialized() {
		log.Fatalln("Error: Plugin not initialized")
	}
	err := os.Mkdir(TmpDir, os.ModePerm)
	path, err := ioutil.TempDir(TmpDir, "")

	if err != nil {
		log.Fatalf("Error: %s", err)
	}
	b := &PluginBuilder{plugin: p, rootPath: path, path: path}

	b.createPluginDir(p.Class)
	path = filepath.Join(path, p.Class)

	b.tplBox = packr.NewBox("../templates/Plugin/")

	b.path = path
	return b
}

type tplSubscriber struct {
	Plugin plugin.Plugin
	Name   string
	Path   string
	Events map[string]string
}

var subscriberDir = "Subscriber"
var resourcesDir = "Resources"

// Build creates the data
func (b *PluginBuilder) Build() {
	p := b.plugin
	b.renderFile("plugin.php.tpl", p.Class+".php", p)
	b.renderFile("plugin.xml.tpl", "plugin.xml", p)
	b.renderSubscribers()
	b.createPluginDir(resourcesDir)
	b.renderFile("Resources/services.xml.tpl", filepath.Join(resourcesDir, "services.xml"), p)

}

// renderSubscribers renders all Subscriber files
func (b *PluginBuilder) renderSubscribers() {
	p := b.plugin
	if len(p.Subscribers) <= 0 {
		return
	}

	b.createPluginDir(subscriberDir)
	var tplSubscriberData tplSubscriber
	for _, s := range p.Subscribers {
		path := filepath.Join(subscriberDir, s.Path)
		b.createPluginDir(path)

		events := make(map[string]string)

		for _, e := range s.Events {
			events[e] = strcase.ToLowerCamel(e)
		}

		tplSubscriberData = tplSubscriber{Plugin: p, Name: s.Name, Path: s.Path, Events: events}

		b.renderFile("Subscriber/subscriber.php.tpl", filepath.Join(path, s.Name+".php"), tplSubscriberData)
	}

}

func (b *PluginBuilder) createPluginDir(name string) {
	err := os.Mkdir(filepath.Join(b.path, name), os.ModePerm)
	if err != nil {
		log.Println(err)
	}
}

func (b *PluginBuilder) renderFile(tplFile, path string, data interface{}) error {
	path = filepath.Join(b.path, path)
	funcMap := template.FuncMap{
		"toCamel":      strcase.ToCamel,
		"toLowerCamel": strcase.ToLowerCamel,
		"toSnake":      strcase.ToSnake,
	}
	name := filepath.Base(tplFile)
	t, err := template.New(name).Funcs(funcMap).Parse(b.tplBox.String(tplFile))
	if err != nil {
		log.Fatalln(err.Error())
	}

	f, err := os.Create(path)
	if err != nil {
		log.Println("create file: ", err)
	}

	log.Debugf("Create File '%s' with template '%s'", path, tplFile)
	//t.Funcs(funcMap)
	err = t.Execute(f, data)
	//err = t.ExecuteTemplate(f, "", data)
	if err != nil {
		log.Println(err)
	}

	f.Close()

	return nil
}

// CleanUp cleans and remove all tmp files
func (b *PluginBuilder) CleanUp() error {
	return os.RemoveAll(b.rootPath)
}
