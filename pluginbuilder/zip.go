package pluginbuilder

import (
	"archive/zip"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// Zip creates a Zip File
func (b *PluginBuilder) Zip() error {
	source := b.path
	zipfile, err := ioutil.TempFile(TmpDir, "swpluginbuilder-"+b.plugin.Class)
	if err != nil {
		return err
	}

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	info, err := os.Stat(source)
	if err != nil {
		return nil
	}

	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}

	filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		if baseDir != "" {
			header.Name = filepath.Join(baseDir, strings.TrimPrefix(path, source))
		}

		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}

		writer, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(writer, file)
		return err
	})

	zipfile.Close()

	zipfilename := filepath.Join(b.rootPath, b.plugin.Class+".zip")

	err = os.Rename(zipfile.Name(), zipfilename)
	b.zipFile = zipfilename

	return err
}

// GetZipFile return the path to zip file
func (b *PluginBuilder) GetZipFile() string {
	return b.zipFile
}
